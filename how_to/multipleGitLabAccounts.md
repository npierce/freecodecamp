[source](https://medium.com/uncaught-exception/setting-up-multiple-gitlab-accounts-82b70e88c437) 

## Generate SSH keys for each user
> ~/.ssh/id_rsa_work
> ~/.ssh/id_rsa_personal
  
## Configure SSH to use the correct keys  
Modify/create ~/.ssh/config file
~~~
Host gitlab.com
    HostName gitlab.com
    User git
    IdentityFile ~/.ssh/personal_key

Host gitlab.com-work
    HostName gitlab.com
    User git
    IdentityFile ~/.ssh/work_key
~~~

## Cloning Repositories
When cloning repos from the (in this example) *work* account, you have to modify the URL's host to match the SSH config's Host directive.
> #Before  
> $ git clone git@gitlab.com:work/repository.git  
>  
> #After  
> $ git clone git@gitlab.com-work:work/repository.git 

## Updating existing repositories
> git remote set-url origin git@gitlab.com-work:work/repository.git   
